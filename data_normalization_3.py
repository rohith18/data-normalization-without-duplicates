from xlsxwriter import Workbook
from fuzzywuzzy import fuzz
from c_name_and_manufacturer_name import c_name_set, manufacturer_name_set

c_name_list_3 = list()
manufacturer_name_list_3 = list()

similar_c_name, similar_manufacturer_name_100, similar_manufacturer_name_95, similar_manufacturer_name_90, similar_manufacturer_name_85 = [[] for _ in range(5)]

for i in c_name_set:
    i = (" ".join(i.split())).lower()
    replaced_str = i
    replacable_strings = {"pvt":"private", "pvt.":"private", "ltd":"limited", "(p)":"private", "&":"and", "co.":"c_name", ",":" ", ".":" ", "-":" "}
    for key, value in replacable_strings.items():
        replaced_str = replaced_str.replace(key, value)
        
    if (("(" and ")") in replaced_str):
        replaced_str = replaced_str.replace(replaced_str[ replaced_str.find("(") : replaced_str.find(")") + 1], "")

    replaced_str = (" ".join(replaced_str.split()))
    c_name_list_3.append(replaced_str)

for i in manufacturer_name_set:
    i = (" ".join(i.split())).lower()
    replaced_str = i
    replacable_strings = {"pvt":"private", "pvt.":"private", "ltd":"limited", "(p)":"private", "&":"and", "co.":"c_name", ",":" ", ".":" ", "-":" "}
    for key, value in replacable_strings.items():
        replaced_str = replaced_str.replace(key, value)
        
    if (("(" and ")") in replaced_str):
        replaced_str = replaced_str.replace(replaced_str[ replaced_str.find("(") : replaced_str.find(")") + 1], "")

    replaced_str = (" ".join(replaced_str.split()))
    manufacturer_name_list_3.append(replaced_str)

print("No. of items in c_name_list_3: ", len(c_name_list_3))
print("No. of items in manufacturer_name_list_3: ", len(manufacturer_name_list_3))

for c_name in c_name_list_3:
    for manufacturer_name in manufacturer_name_list_3:
        if(fuzz.token_sort_ratio(c_name,manufacturer_name) == 100):
            if c_name not in similar_c_name:
                similar_c_name.append(c_name)
                similar_manufacturer_name_100.append(manufacturer_name)
                similar_manufacturer_name_95.append("")
                similar_manufacturer_name_90.append("")
                similar_manufacturer_name_85.append("")

not_similar_c_name_list = [i for i in c_name_list_3 if i not in similar_c_name]
not_similar_manufacturer_name_list = [i for i in manufacturer_name_list_3 if i not in similar_manufacturer_name_100]

for c_name in not_similar_c_name_list:
    for manufacturer_name in not_similar_manufacturer_name_list:
        if ((fuzz.token_sort_ratio(c_name,manufacturer_name) >= 95) and (fuzz.token_sort_ratio(c_name,manufacturer_name) < 100)):
            if c_name not in similar_c_name:
                similar_c_name.append(c_name)
                similar_manufacturer_name_100.append("")
                similar_manufacturer_name_95.append(manufacturer_name)
                similar_manufacturer_name_90.append("")
                similar_manufacturer_name_85.append("")

c_name_list_3.clear()               
manufacturer_name_list_3.clear()

c_name_list_3 = [i for i in not_similar_c_name_list if i not in similar_c_name]
manufacturer_name_list_3 = [i for i in not_similar_manufacturer_name_list if i not in similar_manufacturer_name_95]

for c_name in c_name_list_3:
    for manufacturer_name in manufacturer_name_list_3:
        if ((fuzz.token_sort_ratio(c_name,manufacturer_name) >= 90) and (fuzz.token_sort_ratio(c_name,manufacturer_name) < 95)):
            if c_name not in similar_c_name:
                similar_c_name.append(c_name)
                similar_manufacturer_name_100.append("")
                similar_manufacturer_name_95.append("")
                similar_manufacturer_name_90.append(manufacturer_name)
                similar_manufacturer_name_85.append("")

not_similar_c_name_list.clear() 
not_similar_manufacturer_name_list.clear()

not_similar_c_name_list = [i for i in c_name_list_3 if i not in similar_c_name]
not_similar_manufacturer_name_list = [i for i in manufacturer_name_list_3 if i not in similar_manufacturer_name_90]

for c_name in not_similar_c_name_list:
    for manufacturer_name in not_similar_manufacturer_name_list:
        if ((fuzz.token_sort_ratio(c_name,manufacturer_name) >= 85) and (fuzz.token_sort_ratio(c_name,manufacturer_name) < 90)):
            if c_name not in similar_c_name:
                similar_c_name.append(c_name)
                similar_manufacturer_name_100.append("")
                similar_manufacturer_name_95.append("")
                similar_manufacturer_name_90.append("")
                similar_manufacturer_name_85.append(manufacturer_name)
            
book = Workbook("normalised_data_2.xlsx")
sheet = book.add_worksheet()

sheet.write(0, 0, "c_name_list_3")
sheet.write(0, 1, "manufacturer_name_list_3(100%)")
sheet.write(0, 2, "manufacturer_name_list_3(95%)")
sheet.write(0, 3, "manufacturer_name_list_3(90%)")
sheet.write(0, 4, "manufacturer_name_list_3(85%)")

sheet.write_column(1, 0, similar_c_name)
sheet.write_column(1, 1, similar_manufacturer_name_100)
sheet.write_column(1, 2, similar_manufacturer_name_95)
sheet.write_column(1, 3, similar_manufacturer_name_90)
sheet.write_column(1, 4, similar_manufacturer_name_85)

book.close()